# logitech-circle-downloader

Tired of using circle.logi.com to see your Circle video clips? Want to keep a local archive, but don't want to pay for a cloud storage subscription? Call this script at least once a day to download the individual clips from the command line.

## Installation

Download the project, then create a runner script by copying the `run.example.sh` sample file and adding the username and password, and finally create the `downloads` directory.
```bash
$ npm update
$ cp run.example.sh run.sh
$ mkdir -p downloads
```

## Usage

Start downloading your videos by simply running the `run.sh` script.
```bash
$ run.sh
```

## Project

The original implementation was provided by [gkoehler](https://github.com/gkoehler/LogiCircleDownloader) and further development/bugfixing will be followed by [Martín Claro](https://gitlab.com/martinclaro).

## License

The project is licensed under MIT license (see LICENSE file).
