#!/usr/bin/env bash

export LOGI_EMAIL="my_email"
export LOGI_PASS="my_password"
export DOWNLOAD_DIRECTORY="downloads"

export DEBUG="dsd"

LOGI_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd "${LOGI_PATH}"

node downloader.js

exit ${?}