const debug = require('debug')('dsd');
const fs = require('fs');
const fsxtra = require("fs-extra");
const fetch = require('node-fetch');
const low = require('lowdb')
const FileAsync = require('lowdb/adapters/FileAsync')
const path = require('path');

const get_user_agent = () => {
    return 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36';
}

const get_anti_cache = () => {
    return (new Date().getTime());
}

const authorize = async (user) => {
    let antiCache = get_anti_cache();
    let authResponse = await fetch(`https://video.logi.com/api/accounts/authorization?antiCache=${antiCache}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'User-Agent': get_user_agent(),
            'Origin': 'https://circle.logi.com',
            'Referer': 'https://circle.logi.com/'
        },
        body: JSON.stringify(user)
    });

    let cookie = authResponse.headers.get('set-cookie');
    let sessionCookie = cookie.match(/prod_session=[^;]+/)[0];
    return sessionCookie;
};

const get_accessories = async (sessionCookie) => {
    let antiCache = get_anti_cache();
    return await fetch(`https://video.logi.com/api/accessories?antiCache=${antiCache}`, {
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'User-Agent': get_user_agent(),
            'Cookie': sessionCookie,
            'Origin': 'https://circle.logi.com',
            'Referer': 'https://circle.logi.com/'
        }
    })
    .then(response => response.json());
};

const get_activities = async (accessory, sessionCookie, startActivityId=undefined) => {
    let body = {};
    if(startActivityId != undefined) {
        body = {
            "extraFields": [
                "activitySet"
            ],
            "operator": "<",
            "limit": 99,
            "scanDirectionNewer": true,
            "startActivityId": startActivityId,
            "filter": "relevanceLevel = 0 OR relevanceLevel >= 1"
        };
    } else {
        body = {
            "extraFields": [
                "activitySet"
            ],
            "operator": "<=",
            "limit": 99,
            "scanDirectionNewer": true,
            "filter": "relevanceLevel = 0 OR relevanceLevel >= 1"
        };
    }
    debug(body);

    let antiCache = get_anti_cache();
    let activitiesResponse = await fetch(`https://video.logi.com/api/accessories/${accessory.accessoryId}/activities?antiCache=${antiCache}`,
    {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'User-Agent': get_user_agent(),
            'Cookie': sessionCookie,
            'Origin': 'https://circle.logi.com',
            'Referer': 'https://circle.logi.com/'
        },
        body: JSON.stringify(body)
    })
    .then(response => response.json());

    return activitiesResponse.activities;
};

const download_activity = async(accessory, activity, sessionCookie) => {
    let antiCache = get_anti_cache();
    let url = `https://video.logi.com/api/accessories/${accessory.accessoryId}/activities/${activity.activityId}/mp4?antiCache=${antiCache}`;
    debug(`downloading ${url}`);

    return await fetch(url, {
        headers: {
            'User-Agent': get_user_agent(),
            'Cookie': sessionCookie,
            'Origin': 'https://circle.logi.com',
            'Referer': 'https://circle.logi.com/'
        }
    }).then(response => {
        let contentDisposition = response.headers.get('content-disposition');
        let filename = contentDisposition.match(/filename=([^;]+)/)[1];
        return [filename, response.body];
    });
};

const delete_activity = async(accessory, activity, sessionCookie, db, token) => {
    let antiCache = get_anti_cache();
    let url = `https://video.logi.com/api/accessories/${accessory.accessoryId}/activities/${activity.activityId}?antiCache=${antiCache}`;
    debug('Deleting', url, '...');

    return await fetch(url, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'User-Agent': get_user_agent(),
            'Cookie': sessionCookie,
            'Origin': 'https://circle.logi.com',
            'Referer': 'https://circle.logi.com/'
        }
    })
    .then(response => {
        if (response.status == 204 || response.status == "204") {
            db.get('downloadedActivities').pull(token).write();
            debug("Activity Deleted:", token);
            return true;
        } else {
            debug("Activity *NOT* Deleted:", token);
            debug("response.status:", response.status);
            return false;
        }
    });
};

const save_stream = async(filepath, stream, db, token) => {
    await stream.pipe(fs.createWriteStream(filepath)).on('close', () => {
        db.get('downloadedActivities').push(token).write();
        debug("Activity Saved:", token, '>', filepath);
    });
};

const run = async() => {
    const user = {
        email: process.env.LOGI_EMAIL,
        password: process.env.LOGI_PASS
    };

    const download_directory = process.env.DOWNLOAD_DIRECTORY;
    const db = await low(new FileAsync('db.json'));

    await db.defaults({ downloadedActivities: [] }).write()

    let sessionCookie = await authorize(user);

    let accessories = await get_accessories(sessionCookie);

    let lastActivity = [];

    for(accessory of accessories) {
        debug(`### Processing ${accessory.name}...`);

        lastActivity[ accessory.accessoryId ] = { actId: undefined };

        let activities = await get_activities(accessory, sessionCookie, lastActivity[accessory.accessoryId].actId);

        while(activities.length > 0) {

            for(activity of activities) {

                let token = accessory.accessoryId +'@'+ activity.activityId;

                let found = db.get('downloadedActivities').indexOf(token) > -1;

                debug('Activity Date/Time:', activity.startTime);

                if(!found) {

                    let startDateTime = new Date(activity.startTime);
                    let dateYear = startDateTime.getFullYear().toString();
                    let dateMonth = ((startDateTime.getMonth() + 1) < 10?"0":"") + (startDateTime.getMonth() + 1).toString();
                    let dateDay = (startDateTime.getDate() < 10?"0":"") + startDateTime.getDate().toString();

                    let [filename, stream] = await download_activity(accessory, activity, sessionCookie);
                    let filedir = path.join(download_directory, dateYear, dateMonth, dateDay, accessory.name.replace(/[^a-z0-9\.\-]/gi, '_'));
                    let filepath = path.join(filedir, filename);

                    fsxtra.ensureDirSync(filedir);

                    await save_stream(filepath, stream, db, token);

                } else {

                    await delete_activity(accessory, activity, sessionCookie, db, token);

                }

                lastActivity[ accessory.accessoryId ] = { actId: activity.activityId };

            }

            activities = await get_activities(accessory, sessionCookie, lastActivity[accessory.accessoryId].actId);
        }

    }

};

run()